﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TareasITIC92.Data;
using Xamarin.Forms;

namespace TareasITIC92
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private IList<Tarea> tareas = new ObservableCollection<Tarea>();
        private TareaManager manager = new TareaManager();
        public MainPage()
        {
            BindingContext = tareas;
            InitializeComponent();
            this.recargar();
            recarga.ItemsSource = tareas;
            this.inicio();
        }
        public void inicio()
        {
            recarga.RefreshCommand = new Command(() => {
                recarga.IsRefreshing = true;
                this.recargar();
            });
        }
        async public void OnRefresh(object sender, EventArgs e)
        {
            var tareasCollection = await manager.GetAll();
            foreach (Tarea tarea in tareasCollection)
            {
                if (tareas.All(t => t.Id != tarea.Id))
                {
                    tareas.Add(tarea);
                }
            }
        }

        async public void recargar()
        {
            tareas.Clear();
            var tareasCollection = await manager.GetAll();
            foreach (Tarea tarea in tareasCollection)
            {

                if (tareas.All(t => t.Id != tarea.Id))
                {
                    tareas.Add(tarea);
                }
            }
            recarga.IsRefreshing = false;
        }

        async public void OnAddTarea(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AddTarea(manager));
        }

        async public void OnDelete(object sender, EventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;

            var t = menuItem.CommandParameter as Tarea;

            bool answer = await DisplayAlert("Eliminar", $"¿Seguro que quieres eliminar la tarea {t.Titulo}?", "Sí", "No");

            if (answer)
            {
                await manager.Delete(t.Id);
                tareas.Remove(t);

            }
        }
        private void OnUpdate(object sender, EventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;

            var l = menuItem.CommandParameter as Tarea;

            if (l != null)
            {
                Navigation.PushAsync(new UpdateTarea(manager, l));
            }
        }


    }
}

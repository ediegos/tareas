﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TareasITIC92.Data
{
    public class Tarea
    {
        public long Id { get; set; }
        public string Titulo { get; set; }
        public string Detalle { get; set; }
        public string Responsable { get; set; }
        public string FechaEntrega { get; set; }

        public override string ToString()
        {
            return "Detalle:"  + Environment.NewLine+
                Detalle + " Responsable:" + Responsable + Environment.NewLine +"Fecha de Entrega:" + FechaEntrega;

        }


    }
}

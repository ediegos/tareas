﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TareasITIC92.Data
{
    public class TareaManager
    {
        const string url = "http://192.168.100.52:3000/tareas";

        public async Task<IEnumerable<Tarea>> GetAll()
        {
            HttpClient client = new HttpClient();
            string result = await client.GetStringAsync(url);
            return JsonConvert.DeserializeObject<IEnumerable<Tarea>>(result);
        }

     

        public async Task<Tarea> Add(string titulo, string detalle, string responsable, string fechaEntrega)
        {
            Tarea tarea = new Tarea()
            {
                Titulo = titulo,
                Detalle = detalle,
                Responsable = responsable,
                FechaEntrega = fechaEntrega
       
        };

            HttpClient client = new HttpClient();
            var response = await client.PostAsync(url,
                new StringContent(
                    JsonConvert.SerializeObject(tarea),
                    Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<Tarea>(
                await response.Content.ReadAsStringAsync());

        }

        
        public async Task<int> Delete(long Id)
        {
            HttpClient client = new HttpClient();

            var response = await client.DeleteAsync($"{url}/{Id}");

            return JsonConvert.DeserializeObject<int>(
                await response.Content.ReadAsStringAsync());
        }

        public async Task<Tarea> Update(long Id, string titulo, string detalle, string responsable, string fechaEntrega)
        {
            Tarea tarea = new Tarea()
            {
                Titulo = titulo,
                Detalle = detalle,
                Responsable = responsable,
                FechaEntrega = fechaEntrega
            };

            HttpClient client = new HttpClient();
            var response = await client.PutAsync($"{url}/{Id}",
                new StringContent(
                    JsonConvert.SerializeObject(tarea),
                    Encoding.UTF8, "application/json"));

            return JsonConvert.DeserializeObject<Tarea>(
                await response.Content.ReadAsStringAsync());

        }


    }
}

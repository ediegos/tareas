﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TareasITIC92.Data;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TareasITIC92
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateTarea : ContentPage
    {
        private TareaManager manager;
        private Tarea tarea;
        public UpdateTarea()
        {
            InitializeComponent();
        }


        public UpdateTarea(TareaManager manager, Tarea tarea = null)
        {
            InitializeComponent();

            this.tarea = tarea;
            this.manager = manager;

            cargar(this.tarea);
        }


        private void cargar(Tarea tarea)
        {
            txtTitulo.Text = tarea.Titulo;
            txtDetalle.Text = tarea.Detalle;
            txtResponsable.Text = tarea.Responsable;
            txtFechaEntrega.Text = tarea.FechaEntrega;
        }

        async public void OnUpdateTarea(object sender, EventArgs e)
        {

            await manager.Update(this.tarea.Id, txtTitulo.Text, txtDetalle.Text, txtResponsable.Text, txtFechaEntrega.Text);
            OnBackHome();
           

        }

        async private void OnBackHome()
        {
            await Navigation.PopToRootAsync();

        }


    }

}
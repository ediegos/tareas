'use strict';

var Tarea = require('../models/Tarea');
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', async function (req, res) {
    const tareas = await Tarea.findAll();
    res.json(tareas);
});


router.post('/', async function (req, res) {
    let { Titulo, Detalle, Responsable, FechaEntrega } = req.body;
    let tarea = await Tarea.create({
        titulo: Titulo,
        detalle: Detalle,
        responsable: Responsable,
        fechaEntrega: FechaEntrega
    });
    res.json(tarea);
});

router.put('/:idTarea', async function (req, res) {
    try {
        const { idTarea } = req.params;
        const { Titulo, Detalle, Responsable, FechaEntrega } = req.body;

        let TareaAgregar = await Tarea.update(
            {
                titulo: Titulo,
                detalle: Detalle,
                responsable: Responsable,
                fechaEntrega: FechaEntrega
            },
            {
                where: { id: idTarea }
            }
        );

        if (TareaAgregar.length > 0) {
            let tarea = await Tarea.findOne({ where: { id: idTarea } });
            res.json(tarea);
        }

    } catch (e) {
        res.send(e);
    }
});

router.delete('/:idTarea', async function (req, res) {
    try {
        const { idTarea } = req.params;
        let tarea = await Tarea.destroy({
            where: { id: idTarea }
        });
        console.log(tarea);
        res.json(tarea);
    } catch (e) {
        res.send(e)
    }
    
});




module.exports = router;
